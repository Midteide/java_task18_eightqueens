import java.util.ArrayList;

public class Task18 {

    // public class Queen
    static boolean[][] board;
    static final int N = 8;

    public static void main(String[] args) {
        ArrayList<Solution> solutions = new ArrayList<Solution>() ;
        int inputX=0, inputY=0;
        
        // Handle wrong user input
        if (args.length == 0){
            printInstructions();
            System.exit(0) ;
        }
        if (args.length == 1){
            printInstructions();
            System.exit(0) ;
        }

        // convert coordinates to numbers instead of letters
        // If outside boundry of A-G then print instructions and kill program
        switch( args[0].toLowerCase().charAt(0)){
            case 'a': inputX = 0;
            break;
            case 'b': inputX = 1;
            break;
            case 'c': inputX = 2;
            break;
            case 'd': inputX = 3;
            break;
            case 'e': inputX = 4;
            break;
            case 'f': inputX = 5;
            break;
            case 'g': inputX = 6;
            break;
            case 'h': inputX = 7;
            break;
            default:    printInstructions();
                        System.exit(0) ;
                        break;
        }
        inputY = Integer.parseInt(args[1])-1;

        // If outside boundry of 1-8 then print instructions and kill program
        if ((inputY > 8) || (inputY < 1 )) {
            printInstructions();
            System.exit(0) ;
        }

        print("Please wait while I look for a solution...");
        
        Solution solution = new Solution();
        solution = getSolution(inputX, inputY);
        solutions.add(solution);
        print("");
        print("Found a solution! It looks like this:");
        solution.print();
        
        print("Please wait while I look for more solutions...");
       

        // Run through 10 more iterations to check for new unique solutions
        for (int i=0;i<10;i++){
            print("");
            if (i<2) System.out.print("Looking...");
            else if (i<4) System.out.print("Looking some more...");
            else if (i<6) System.out.print("Still looking...");
            else if (i<7) System.out.print("Grab a coffee, I'm soon finished...");
            else if (i<8) System.out.print("Finishing touches now...");
            solution = getSolution(inputX, inputY);
            if (!isDuplicate(solution,solutions)) {
                solutions.add(solution);
            }
        }
        System.out.print("Done!");
        System.out.println("___________________________");
        System.out.println("Printing solutions:");
        int solutionNumber =1;
        for (Solution s : solutions) {
            System.out.println("Solution number: " + solutionNumber);
            s.print();
            solutionNumber++;
        }
    }
    public static void printInstructions() {
        System.out.println("Please provide arguments in following format: 'java Task18.java <column> <row>'");
        System.out.println("Example: 'java Task18.java g 5'");
    }

    // Just to not have to type "System.out.println" all the time
    public static void print(String s) {
        System.out.println(s);
    }


    // Checks if provided solution is already in provided ArrayList of solutions
    public static boolean isDuplicate(Solution solution, ArrayList<Solution> solutions){
        boolean isDuplicate = false;
        for (Solution s : solutions) {
            isDuplicate=true;
            for (int i=0; i<8; i++) {
                if (s.squares[i] != solution.squares[i]) isDuplicate= false;
            }
            if (isDuplicate) break;
        }
        return isDuplicate;
    }

    // Takes provided coordinates and looks for a solution and the returns it as a Solution object
    public static Solution getSolution(int x, int y) {
        boolean conflict = false;
        long counter =0;
        long c2=0;

        // Creating an array of eight Queen objects, which represents the eight queens placed on the board
        Queen queens[] = new Queen[8];
        for (int i=0;i<8;i++) queens[i] = new Queen();
        for (int k=0;k<8;k++){
            for (int l=0;l<8;l++) {
                queens[k].setPosition(k, l); 
            }
        }

        // First position in array is "sacred", and set to user input square coordinates.
        // Solution will have to be found without changing this value
        queens[0].setPosition(y,x);
        for (int i=1;i<8;i++){
            conflict = false;
            
            while (true){
                counter++;
                conflict = false;
                c2++;

                // Show user that program is working
                if (c2 > 30000) {
                    System.out.print(".");
                    c2=0;
                }
                
            
                for (int row=0; row <= i; row++) {

                    // If no solution is found within a given time then reinitialize queens to new
                    // random poistions and try again
                    if ((counter > 50)) {
                        row =0;
                        i=0;
                        counter =0;
                        for (int k=1;k<8;k++){
                            for (int l=0;l<8;l++) {
                                queens[k].setPosition((1+(int)(7.0 * Math.random())), ((int)(7.0 * Math.random()))); 
                            }
                        }
                    }
                    
                    // Check if any of the queens are attacking eachother, and if a queen is attacking
                    // another then queen then give that queen a new position and continue trying.
                    for (int col=0; col <= i; col++){
                        if (col == row) continue; // No point of checking queen against itself
                        if (checkIfQueensAttackEachother(queens[row].attacks, queens[col].attacks)) {
                            conflict = true;
                            if((row == 0) && (col == 0)) queens[(1+(int)(7.0 * Math.random()))].nextPosition();
                            else if ((row == 0)) queens[col].nextPosition();
                            else  queens[row].nextPosition();
                        }
                    }
                }   
                // If none of the queens attack eachother then the solution is done and we break the while loop
                if (!conflict) {
                break;
                }
            }
        }

        int retArray[] = new int[8];
        // Saving queen positions where array placement is row and value is column
        for (Queen queen : queens) {
            if (queen.row == 0) retArray[0] = queen.col;
            if (queen.row == 1) retArray[1] = queen.col;
            if (queen.row == 2) retArray[2] = queen.col;
            if (queen.row == 3) retArray[3] = queen.col;
            if (queen.row == 4) retArray[4] = queen.col;
            if (queen.row == 5) retArray[5] = queen.col;
            if (queen.row == 6) retArray[6] = queen.col;
            if (queen.row == 7) retArray[7] = queen.col;
        }
        Solution retSolution = new Solution(retArray);
        return retSolution;
    }

    public static boolean checkIfQueensAttackEachother(char [][] a, char[][] b){
        boolean attacks = false;
        for (int row=0; row < 8; row++) {
                    
            for (int col=0; col<8; col++){
                // Check if queen attacks eachother
                if ((a[row][col] == 'X') && (b[row][col] == 'Q')) attacks = true;  
                // Check if queen attacks eachother
                if ((a[row][col] == 'Q') && (b[row][col] == 'X')) attacks = true;
                // Check if queen is placed at the same square
                if ((a[row][col] == 'Q') && (b[row][col] == 'Q')) attacks = true;
            }
        }
        return attacks;
    }

}
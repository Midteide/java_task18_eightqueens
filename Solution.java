public class Solution {
    
    public Solution (){
        for (int i=0;i<8;i++)this.squares[i] = 0;
    }
    public Solution (int[] s){
        for (int i=0;i<8;i++)this.squares[i] = s[i];
    }

    public void print(){
        char board[][] = new char[8][8];
        // System.out.println("Original:");
        for (int i=0; i<8; i++)
            for (int j=0;j<8;j++) 
            {
                if (this.squares[i] == j) board[i][j] = 'Q';
            }
        drawBoard(board);
    }

    

    public static void drawBoard(char b[][]) {
        char board[][] = new char[8][8];

        // Used to draw different symbol for white/black squares (not currently in use)
        boolean whiteSquare = false; 
        
        for (int row=0; row < 8; row++) {

            for (int col=0; col<8; col++){
                
                if (whiteSquare) board[row][col] = '_';
                else board[row][col] = '_';
                whiteSquare = !whiteSquare;
                if (b[row][col] == 'Q'){
                    board[row][col] = 'Q';
                }
            }
        }
        System.out.println("   A  B  C  D  E  F  G  H  ");
        // board = b;
        for (int row=7; row >= 0; row--) {
            System.out.print(row+1);
            
            for (int col=0; col<8; col++){
                System.out.print("  ");
                System.out.print(board[row][col]);
                
            }
            System.out.println("  "+(row+1));
        }
        System.out.println("   A  B  C  D  E  F  G  H  ");
        System.out.println("");
    }
    public int squares[] = new int[8];
}